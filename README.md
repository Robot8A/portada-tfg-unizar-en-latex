# Portada TFG Unizar en LaTeX

1. Sustituir texto entre guiones en portada.tex por los parámetros correspondientes
2. Correr `pdflatex portada.tex` para generar la portada en PDF (https://www.tug.org/applications/pdftex/)
3. Usar
 
Ejemplo de TFG en LaTeX:
https://gitlab.com/Robot8A/tfg-trafair-unizar